const password = document.getElementById('password-id');
const password2 = document.getElementById('password2-id');
const eye = document.getElementById('eye-id');
const eye2 = document.getElementById('eye2-id');
const button = document.getElementById('button-id');
const error = document.getElementById('error-id');

eye.addEventListener('click', event => {
    const inputType = password.getAttribute("type");
    if (inputType === "password") {
        eye.classList.remove("fa-eye");
        eye.classList.add("fa-eye-slash");
        password.setAttribute('type', "text");
    } else {
        eye.classList.remove("fa-eye-slash");
        eye.classList.add("fa-eye");
        password.setAttribute("type", "password");
    }
})

eye2.addEventListener('click', event => {
    const inputType = password2.getAttribute("type");
    if (inputType === "password") {
        eye2.classList.remove("fa-eye");
        eye2.classList.add("fa-eye-slash");
        password2.setAttribute('type', "text");
    } else {
        eye2.classList.remove("fa-eye-slash");
        eye2.classList.add("fa-eye");
        password2.setAttribute("type", "password");
    }
})

button.addEventListener('click', event => {
  event.preventDefault();
  if (password.value === password2.value) {
      error.innerHTML = ""
      alert("You are welcome");
  } else {
    error.innerHTML = "Have to enter the same values"
  }
})

